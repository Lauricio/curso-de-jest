
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TextInput  } from ".";


describe("<textInput/>", () =>{

  it("should have value of searchValue",()=>{
    const fn = jest.fn();
    render(<TextInput handleClick={fn} 
      search={"testando"} /> );

    const input = screen.getByPlaceholderText(/pesquisar/i);
    expect(input).toBeInTheDocument();

    expect(input.value).toBe("testando")

  });



  it("should call handleChange function textInput",()=>{
    const fn = jest.fn();
    render(<TextInput handleClick={fn}  /> );

    const input = screen.getByPlaceholderText(/pesquisar/i);

    const value = "valor";

    userEvent.type(input, value);
    expect(input.value).toBe(value);
    expect(fn).toHaveBeenCalledTimes(value.length)


  });


});
