import { render, screen } from '@testing-library/react';
import { Button } from '../../components/Button';

describe("<home/>", () =>{
  it("should render button the  text 'load more' ", ()=>{
    render(<Button text="load more"/>);

    expect.assertions(1);
    //quantidade de asserções/execuções caso seja execultado mais vezes do que esperado ira retornar erro

    const button = screen.getByRole("button", { name: /load more/i});
   //acima definie button que deve conter o texto "loaf more"

    expect(button).toBeInTheDocument();
    //a  expressão acima apenas indica que o elemento "button" deve estar no documento "DOM"
    
  })
 
});

