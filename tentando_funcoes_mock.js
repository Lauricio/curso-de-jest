
import { fireEvent, render, screen } from "@testing-library/react";
import { Button } from "../../components/Button";

describe("<home/>", () => {
  it("should render button the  text 'load more'", () => {
    render(<Button text="load more" />);

    expect.assertions(1);
    //quantidade de asserções/execuções caso seja execultado mais vezes do que esperado ira retornar erro

    const button = screen.getByRole("button", { name: /load more/i });
    //acima definie button que deve conter o texto "loaf more"

    expect(button).toBeInTheDocument();
    //a  expressão acima apenas indica que o elemento "button" deve estar no documento "DOM"
  });

  it("should call function on button click", () => {
    const fn = jest.fn();
    render(<Button text="load more" onClick={fn} />);

    const button = screen.getByRole("button", { nome: /load more/i });

    fireEvent.click(button);

    expect(fn).toHaveBeenCalled();
  });

  it("should be disabled when disabled is true ", () => {
    render(<Button text="load more"  disabled={true} />);
    const button = screen.getByRole("button", { nome: /load more/i });
    expect(button).toBeDisabled();
  });
  //teste se o botão está desativado

  it("should be disabled when disabled is false ", () => {
    render(<Button text="load more"  disabled={false} />);
    const button = screen.getByRole("button", { nome: /load more/i });
    expect(button).toBeEnabled();
    
  });
  // teste se o botão está ativado




  





});
